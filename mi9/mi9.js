// vim: et ts=4 sw=4 sts=4 ai si nowrap nu
var http = require('http');

// helper function to handle status codes and json stringification of data
var renderRsp = function(rsp, code, json) {
    rsp.writeHead(code, http.STATUS_CODES[code], {'Content-Type': 'application/json', 'Connection': 'close'});
    rsp.end(JSON.stringify(json));
    console.log("%d %s", code, http.STATUS_CODES[code]);
    return rsp;
};

http.createServer(function(req, rsp) {
    console.log(new Date)
    console.log('%s request from %s', req.method, req.connection.remoteAddress);
    // coding challenge states "we'll post some JSON data", behind the scenes states the request will be a PUT.
    //    Lets handle both the same way.
    if (req.method == 'POST' || req.method == 'PUT') {

        // If transfer-encoding is chunked and the data splits, buffer it and try recombine later.
        var data = [];
        req.on('data', function(d) {
            data.push(d);
        });

        // On request read end, try to recombine and parse data as JSON.
        req.on('end', function() {
            console.log('Chunks received: %d (%d bytes)', data.length, req.connection.bytesRead);
            try {
                var json = JSON.parse(data.join(''));
                    json.payload = json.payload || []; // ensure a valid state for filter to run against.


                // filter episodes with drm (implicitly "thruthy") and episodeCount > 0,
                //    then map them to an object literal of image, slug and title.
                return renderRsp(rsp, 200, {
                    response: json.payload.filter(function (ep) {
                        return ep.drm && ep.episodeCount > 0;
                    }).map(function (ep) {
                        return {image: ep.image.showImage, slug: ep.slug, title: ep.title};
                    })
                });

            } catch(ex) {
                console.error(ex);
                return renderRsp(rsp, 400, {error: 'Could not decode request: JSON parsing failed'});
            }

        });
    } else {
        return renderRsp(rsp, 405, {error: 'Could not handle request: Method not implemented'});
    }

}).listen(80); // Listen on 80, privileged ports require escalated privileges.
